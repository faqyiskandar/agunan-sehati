<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AgunanController@home');

Route::get('/products', 'AgunanController@viewProduct');

Route::get('/edit/{id}', 'AgunanController@edit');

Route::post('/store', 'AgunanController@store');

Route::patch('/update/{id}', 'AgunanController@update');

Route::delete('/delete/{id}', 'AgunanController@delete');
