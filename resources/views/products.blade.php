<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Our Agunans</title>
    <link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary">
        <a class="navbar-brand" href="#">MY AGUNAN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="/">Tambah Agunan</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="/products">Lihat Agunan</a>
            </li>
          </ul>
        </div>
      </nav>
    <div class="container mt-5">
        <h1>List Agunan</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No BA</th>
                    <th>Nama</th>
                    <th>Jenis Agunan</th>
                    <th>Tanggal</th>
                    <th>Brangkas</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $agunan)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$agunan->no_ba}}</td>
                    <td>{{$agunan->nama}}</td>
                    <td>{{$agunan->jenis_agunan}}</td>
                    <td>{{$agunan->tanggal}}</td>
                    <td>{{$agunan->brangkas}}</td>
                    <td>
                      <a href="/edit/{{$agunan->id}}" class="btn btn-success">Edit</a>
                      <form action="/delete/{{$agunan->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
    </div>  
</body>
</html>