<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MY AGUNAN</title>
    <link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('style/style.css')}}">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary">
        <a class="navbar-brand" href="#">MY AGUNAN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="/">Tambah Agunan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/products">Lihat Agunan</a>
            </li>
          </ul>
        </div>
      </nav>
    <div class="container mt-5" style="width: 33%;">
        <form class="mb-4" action="/store" method="POST">
            @csrf
            <h1 class="text-center mb-4">Tambah Agunan</h1>
            <div class="form-group">
                <label for="">No BA</label>
                <input type="number" class="form-control" name="no_ba">
            </div>
            <div class="form-group">
                <label for="">Nama</label>
                <input type="text" class="form-control" name="nama">
            </div>
            <div class="form-group">
                <label for="">Jenis Agunan</label>
                <input type="text" class="form-control" name="jenis_agunan">
            </div>
            <div class="form-group">
                <label for="">Tanggal</label>
                <input type="date" class="form-control" name="tanggal">
            </div>
            <div class="form-group">
                <label for="">Brangkas</label>
                <input type="text" class="form-control" name="brangkas">
            </div>  
            <div class="mb-3">
  <label for="formFile" class="form-label">Upload File</label>
  <input class="form-control" type="file" id="formFile">
</div>
            <button type="submit" id="btn-submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>
<script src="{{asset('scripts/jquery-3.5.0.min.js')}}"></script>
<script src="{{asset('scripts/bootstrap.min.js')}}"></script>
</body>
</html>