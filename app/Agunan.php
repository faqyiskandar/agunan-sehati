<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agunan extends Model
{
    protected $fillable = ['no_ba', 'nama', 'jenis_agunan', 'tanggal', 'brangkas'];
}
