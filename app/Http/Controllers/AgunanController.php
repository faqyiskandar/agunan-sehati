<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agunan;

class AgunanController extends Controller
{
    public function home()
    {
        return view('create');
    }

    public function store(Request $request)
    {   
        Agunan::create([
            'no_ba' => $request->no_ba,
            'nama' => $request->nama,
            'jenis_agunan' => $request->jenis_agunan,
            'tanggal' => $request->tanggal,
            'brangkas' => $request->brangkas
        ]);

        return back();
    }

    public function viewProduct()
    {
        $products = Agunan::all();
        return view('products', compact('products'));
    }

    public function edit($id)
    {
        $agunan = Agunan::where('id', $id)->first();
        return view('edit', compact('agunan'));
    }

    public function update(Request $request, $id)
    {
        Agunan::where('id', $id)->update([
            'no_ba' => $request->no_ba,
            'nama' => $request->nama,
            'jenis_agunan' => $request->jenis_agunan,
            'tanggal' => $request->tanggal,
            'brangkas' => $request->brangkas  
        ]);

        return redirect('/products');
    }
    public function delete($id)
    {
        Agunan::destroy($id);
        return back();
    }
}
